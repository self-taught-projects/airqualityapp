export const GREEN_COLOR = { color: '#04dead', statement: 'Good' };
export const YELLOW_COLOR = { color: '#ffcc00', statement: 'Moderate' };
export const ORANGE_COLOR = {
    color: '#fe9801',
    statement: 'Unhealthy for Sensitive Groups',
};
export const RED_COLOR = { color: '#fa163f', statement: 'Unhealthy' };
export const PURPLE_COLOR = { color: '#4a47a3', statement: 'Very Unhealthy' };
export const PIGGY_BLOOD_COLOR = { color: '#851d41', statement: 'Hazardous' };
