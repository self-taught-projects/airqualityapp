import CardReducer from './CardReducer';

const RootReducer = {
    cards: CardReducer,
};

export default RootReducer;
