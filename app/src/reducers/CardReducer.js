import {
    FETCH_DATA,
    FETCH_DATA_FAILURE,
    FETCH_DATA_SUCCESS,
} from '../constants/CardConstant';
const INITIALIZED_STATE = {
    data: {},
    status: '',
    isLoading: false,
    isFetchingError: false,
    city: {},
    time: {},
};
const fetchData = (state, action) => {
    return {
        ...state,
        data: action.data,
    };
};

const fetchDataFailed = (state, action) => {
    return {
        ...state,
        isFetchingError: action.isFetchingError,
    };
};

const fetchDataSuccess = (state, action) => {
    return {
        ...state,
        isFetchingError: action.isFetchingError,
    };
};

export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case FETCH_DATA:
            return fetchData(state, action);
        case FETCH_DATA_FAILURE:
            return fetchDataFailed(state, action);
        case FETCH_DATA_SUCCESS:
            return fetchDataSuccess(state, action);
        default:
            return state;
    }
};
