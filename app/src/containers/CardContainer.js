import { connect } from 'react-redux';
import Cards from '../components/sub-components/Cards';
import { fetchData } from '../actions/CardAction';

const mapStateToProps = state => {
    const { cards } = state;
    const { isFetchingError, data } = cards;
    return {
        isFetchingError,
        data: data.data,
        status: data.status,
        city: data.data.city,
        time: data.data.time,
    };
};

const mapDispatchToProps = (dispatch: Function) => {
    return {
        fetchData: (toSearchCity: String) => {
            dispatch(fetchData(toSearchCity));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
