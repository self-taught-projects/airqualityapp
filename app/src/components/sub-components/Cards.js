import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import _ from 'lodash';
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Thumbnail,
    Text,
    Icon,
    Left,
    Body,
    Item,
    Button,
    Input,
} from 'native-base';
import {
    GREEN_COLOR,
    YELLOW_COLOR,
    ORANGE_COLOR,
    RED_COLOR,
    PURPLE_COLOR,
    PIGGY_BLOOD_COLOR,
} from '../../constants/ActionConstant';
const logo = require('../../../assets/logo.png');

class Cards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            search: '',
        };
    }
    componentDidMount() {
        this.props.fetchData(this.state.toSearch);
    }

    _onSearchPress(city) {
        const { fetchData } = this.props;
        const searchingField = _.replace(_.snakeCase(city), '_', '');
        fetchData(searchingField);
        this._onSearchingFieldClear();
        this.setState({ toSearch: searchingField });
        console.log(searchingField);
    }

    _onSearchingFieldClear() {
        this.setState({ search: '' });
    }

    _onBackgroundColorAirQuality(aqi) {
        if (aqi <= 50) {
            //this.setState({ statement: 'Good' });
            return GREEN_COLOR;
        } else if (aqi <= 100) {
            //this.setState({ statement: 'Moderate' });
            return YELLOW_COLOR;
        } else if (aqi <= 150) {
            //this.setState({ statement: 'Unhealthy for Sensitive Groups' });
            return ORANGE_COLOR;
        } else if (aqi <= 200) {
            // this.setState({ statement: 'Unhealthy' });
            return RED_COLOR;
        } else if (aqi <= 300) {
            //this.setState({ statement: 'Very Unhealthy' });
            return PURPLE_COLOR;
        } else {
            //this.setState({ statement: 'Hazardous' });
            return PIGGY_BLOOD_COLOR;
        }
    }

    render() {
        const { data, isFetchingError, city, time } = this.props;
        const { search } = this.state;
        const {
            particulateMatterText,
            particulateMatter,
            statementText,
            cityText,
        } = styles;
        const color = this._onBackgroundColorAirQuality(data.aqi).color;
        const statement = this._onBackgroundColorAirQuality(data.aqi).statement;
        console.log(isFetchingError);
        console.log('color', this._onBackgroundColorAirQuality(data.aqi));

        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <Icon name="ios-search" />
                        <Input
                            placeholder="Search"
                            name="search"
                            autoComplete={false}
                            value={this.state.search}
                            onChangeText={value =>
                                this.setState({ search: value })
                            }
                        />
                        <Icon name="ios-people" />
                    </Item>
                    <Button
                        transparent
                        onPress={() => this._onSearchPress(search)}
                    >
                        <Text>Search </Text>
                    </Button>
                </Header>
                <Content>
                    {_.isEqual(isFetchingError, true) ? (
                        <Card>
                            <CardItem cardBody style={particulateMatter}>
                                <Text style={particulateMatterText}>404</Text>
                            </CardItem>
                        </Card>
                    ) : (
                        <Card>
                            <CardItem>
                                <Left>
                                    <Thumbnail source={logo} />
                                    <Body>
                                        <Text style={cityText}>
                                            {city.name}
                                        </Text>
                                        <Text note>Updated: {time.s}</Text>
                                    </Body>
                                </Left>
                            </CardItem>
                            <CardItem
                                cardBody
                                style={[
                                    particulateMatter,
                                    {
                                        backgroundColor: color,
                                    },
                                ]}
                            >
                                <Text style={particulateMatterText}>
                                    {data.aqi}
                                </Text>
                                <Text style={statementText}>{statement}</Text>
                            </CardItem>
                        </Card>
                    )}
                </Content>
            </Container>
        );
    }
}

export default Cards;

const styles = StyleSheet.create({
    particulateMatter: {
        flex: 1,
        width: null,
        height: 250,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#bb8fa9',
    },
    particulateMatterText: {
        fontSize: 100,
        fontWeight: 'bold',
        color: '#bb8fa9',
    },
    statementText: {
        fontSize: 35,
        fontWeight: 'bold',
        color: '#bb8fa9',
    },
    cityText: {
        fontWeight: 'bold',
        color: '#bb8fa9',
    },
});
