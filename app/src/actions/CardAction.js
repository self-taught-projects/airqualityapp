import _ from 'lodash';
import { API_COUNTRY_STATE } from '../../utils/Api';
import {
    FETCH_DATA,
    FETCH_DATA_FAILURE,
    FETCH_DATA_SUCCESS,
    BACKGROUND_COLOR_FILTER,
} from '../constants/CardConstant';
import {
    GREEN_COLOR,
    YELLOW_COLOR,
    ORANGE_COLOR,
    PURPLE_COLOR,
    PIGGY_BLOOD_COLOR,
} from '../constants/ActionConstant';

export const fetchData = (toSearchCity: String) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            const city = _.isEmpty(toSearchCity) ? 'chiangmai' : toSearchCity;
            await fetch(
                `https://api.waqi.info/feed/${city}/?token=8638fa92f26fee98a92522e5d9d492be9079bc2b`
            )
                .then(response => response.json())
                .then(data => {
                    console.log('action data', data);
                    if (_.isEqual(data.status, 'ok')) {
                        dispatch({ type: FETCH_DATA, data: data });
                        dispatch({
                            type: FETCH_DATA_SUCCESS,
                            isFetchingError: false,
                        });
                    } else {
                        dispatch({
                            type: FETCH_DATA_FAILURE,
                            isFetchingError: true,
                        });
                    }
                })
                .catch(error => {
                    dispatch({
                        type: FETCH_DATA_FAILURE,
                        isFetchingError: true,
                    });
                    console.warn(error);
                });
        } catch (error) {
            dispatch({ type: FETCH_DATA_FAILURE, isFetchingError: true });
            console.warn(error);
        }
    };
};
