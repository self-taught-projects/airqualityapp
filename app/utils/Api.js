// requires state and country name as input
// display the PM 2.5 of the specific state of the country
export const API =
  'api.airvisual.com/v2/cities?state=New York&country=USA&key=d6282354-6517-4be4-bb61-d79607a39ef6';
// requires country name as input
// display a list of state of the country
export const API_COUNTRY_STATE =
  'api.airvisual.com/v2/states?country=thailand&key=d6282354-6517-4be4-bb61-d79607a39ef6';
// display a list of countries PM 2.5 data available
export const API_COUNTRY =
  'api.airvisual.com/v2/countries?key=d6282354-6517-4be4-bb61-d79607a39ef6';
