import { persistStore, persistCombineReducers } from 'redux-persist';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import RootReducer from '../src/reducers/RootReducer';
import AsyncStorage from '@react-native-community/async-storage';

import { applyMiddleware, createStore } from 'redux';

export const configureStore = () => {
    const persistConfig = {
        key: 'root',
        storage: AsyncStorage,
        whilelist: ['cards'],
    };
    const persistedReducer = persistCombineReducers(persistConfig, RootReducer);
    const middleware = [thunk];
    middleware.push(logger);
    const store = createStore(persistedReducer, applyMiddleware(...middleware));
    const persistor = persistStore(store);
    return {
        store,
        persistor,
    };
};
