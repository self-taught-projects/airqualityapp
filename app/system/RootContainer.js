import { connect } from 'react-redux';
import Root from './Root';
import { fetchData } from '../src/actions/CardAction';

const mapStateToProps = state => {
    const { cards } = state;
    const { isFetching } = cards;
    return {
        isFetching,
    };
};

const mapDispatchToProps = (dispatch: Function) => {
    return {
        fetchData: () => {
            dispatch(fetchData());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Root);
