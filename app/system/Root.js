import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import CardContainer from '../src/containers/CardContainer';

class Root extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <Scene key="root">
                    <Scene
                        key="cards"
                        component={CardContainer}
                        hideNavBar
                        initial
                    />
                </Scene>
            </Router>
        );
    }
}
export default Root;
