/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { configureStore } from './system/ConfigureStore';
import RootContainer from './system/RootContainer';

const { store, persistor } = configureStore();

class App extends Component<{}> {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <RootContainer />
                </PersistGate>
            </Provider>
        );
    }
}

export default App;
